var User = require('../models/user')

module.exports = function user (req, res, next) {
  if (req.session.user) {
    let inicio = Date.now()
    console.log(inicio)
    User.findOne({ _id: req.session.user }).populate('languages')
      .then(user => {
        if (!user.active) {
          req.session.flash = 'Você precisa ativar a sua conta'
          return res.redirect('/login')
        }
        var milis = Date.now() - inicio
        console.log('tempo: ' + milis + 'ms')
        req.user = user
        next()
      })
  } else {
    return res.redirect('/login')
  }
}
