var User = require('../models/user')

module.exports = function admin (req, res, next) {
  if (req.session.user) {
    User.findOne({ _id: req.session.user }, function (err, user) {
      if (err) { res.send(err) }
      req.user = user
      if (user.is_admin) {
        next()
      } else {
        console.log('redirect no admin')
        res.redirect('/login')
      }
    })
  } else {
    console.log(req.session.user)
    console.log('redirect no user')
    res.redirect('/login')
  }
}
