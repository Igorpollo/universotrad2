var createError = require('http-errors')
var express = require('express')
var path = require('path')
var cookieParser = require('cookie-parser')
var logger = require('morgan')
var nunjucks = require('nunjucks')
var mongoose = require('mongoose')
var session = require('express-session')
const MongoStore = require('connect-mongo')(session)
var cron = require('node-cron')
var moment = require('moment')
var passport = require('passport')
var FacebookStrategy = require('passport-facebook').Strategy
var Translation = require('./models/translation')
var config = require('./config')
var socketIo = require('socket.io')

var indexRouter = require('./routes/index')
var usersRouter = require('./routes/users')
var translationsRouter = require('./routes/translation')
var adminRouter = require('./routes/admin')
var statsRouter = require('./routes/stats')
var languageRouter = require('./routes/language')
var ticketRouter = require('./routes/ticket')

var app = express()
var io = socketIo()
app.io = io
require('dotenv').config()

if (!process.env.NODE_ENV) { process.env.NODE_ENV = 'local' }
console.log(process.env.NODE_ENV)
// view engine setup
nunjucks.configure('views', {
  autoescape: true,
  express: app
})

// connect to mongoose
//mongoose.connect('mongodb://pollo:tbi729@ds147534.mlab.com:47534/universo', {useNewUrlParser: true})

if (process.env.NODE_ENV === 'production') {
  mongoose.connect('mongodb+srv://pollinho:tbi729@universo.anc6d.mongodb.net/universo?retryWrites=true&w=majority', {useNewUrlParser: true})
} else {
  mongoose.connect('mongodb://pollo:tbi729@ds331548.mlab.com:31548/universo-dev', {useNewUrlParser: true})
}
// mongoose.set('debug', true)
var d = new Date()
d.setFullYear(2022)

// set session options
app.use(session({
  secret: 'keyboard cat2',
  resave: true,
  saveUninitialized: true,
  store: new MongoStore({ mongooseConnection: mongoose.connection }),
  cookie: { expires: d }
}))

passport.use(new FacebookStrategy({
  clientID: config.facebook_api_key,
  clientSecret: config.facebook_api_secret,
  callbackURL: 'https://plataforma.universotraducoes.com/auth/facebook/callback',
  state: true,
  profileFields: ['id', 'displayName', 'photos', 'email']
},
function (accessToken, refreshToken, profile, cb) {
  console.log(profile)
  return cb(null, profile)
}
))

app.use(function (req, res, next) {
  if (req.session.flash) {
    app.locals.flash = req.session.flash
    req.session.flash = null
  } else {
    app.locals.flash = null
  }
  next()
})

app.use(passport.initialize())
app.use(logger('dev'))
app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(cookieParser())
app.use(express.static(path.join(__dirname, 'public')))
app.use(express.static(path.join(__dirname, 'uploads')))

app.use('/', indexRouter)
app.use('/users', usersRouter)
app.use('/translations', translationsRouter)
app.use('/admin', adminRouter)
app.use('/admin/stats', statsRouter)
app.use('/languages', languageRouter)
app.use('/tickets', ticketRouter)

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404))
})

cron.schedule('*/10 * * * *', async () => {
  console.log('Rodando check premium')
  var todos = await Translation.find({public: false})
  todos.forEach(async translation => {
    if (moment().isAfter(moment(translation.created_at).add(2, 'h'))) {
      await Translation.updateOne({_id: translation._id}, {public: true})
    }
  })
})


cron.schedule('*/1 * * * *', async () => {
  var todos = await Translation.find({status: "Aguardando Aprovação"})
  todos.forEach(async translation => {
    console.log(translation.completed_at)
    if (moment().isAfter(moment(translation.completed_at).add(7, 'd'))) {
      console.log(translation._id)
      await Translation.updateOne({_id: translation._id}, {status: "Concluído"})
    }
  })
})

cron.schedule('*/1 * * * *', async () => {
  var todos = await Translation.find({no_prazo: undefined})
  todos.forEach(async translation => {
    let noPrazo
    if (translation.completed_at) {
      noPrazo = moment(translation.completed_at).isBefore(translation.deadline)
    }
    await Translation.updateOne({_id: translation._id}, {no_prazo: noPrazo})
  })
})

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message
  res.locals.error = req.app.get('env') === 'development' ? err : {}

  // render the error page
  res.status(err.status || 500)
  res.json({
    message: err.message,
    error: err
  })
})

io.on('connection', function (socket) {
  console.log('A user connected')
})
module.exports = app
