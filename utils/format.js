module.exports = {
  format: function (number) {
    let value = number.toString()
    let teste = value.slice(0, -2) + '.' + value.slice(-2)
    let newvalue = Number(teste)
    let opa = newvalue.toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' })
    const reformat = s => s.replace(/[,.]/g, x => ({'.': ',', ',': '.'})[x])
    return reformat(opa)
  },
  format2: function (number) {
    let value = Number(number)
    let opa = value.toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' })
    const reformat = s => s.replace(/[,.]/g, x => ({'.': ',', ',': '.'})[x])
    return reformat(opa)
  }
}
