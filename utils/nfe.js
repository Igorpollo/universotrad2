var nfe = require('nfe-io')('rdwzECp4VDKKZ1e5HAbqhoOm8DPO1CDs2JPAM8N1MVEzzWeF3iGKm0rsDFkmOka57gM')
module.exports = function (name, email, price) {
  let preco = price.replace(',', '.')
  nfe.serviceInvoices.create('5c73cee34c0f50089cc2aad3',
    {
      'cityServiceCode': '3123',
      'description': 'Prestaçao de serviços de traduçao',
      'servicesAmount': preco,
      'borrower': {
        'name': name,
        'email': email,
        'address': {
          'country': 'BRA',
          'postalCode': '70073901',
          'street': 'Outros Quadra 1 Bloco G Lote 32',
          'number': 'S/N',
          'additionalInformation': 'QUADRA 01 BLOCO G',
          'district': 'Asa Sul',
          'city': {
            'code': '5300108',
            'name': 'Brasilia'
          },
          'state': 'DF'

        }
      }
    }, function (err, invoice) {
      if (err) { console.log(err) }
      console.log(invoice)
    }
  )
}
