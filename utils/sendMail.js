var nodemailer = require('nodemailer')

module.exports = function (to, subject, body) {
  let mail = `
  <body>
  <head>
  <style>
  h3 {
    color: #81458f;
    margin-top: 40px;
  }
  a {
    color: #81458f;
  }
  </style>
  </head>
  <div style="background-color:#f7f7f7; font-family: sans-serif;">
      <div style="margin:0px auto;max-width:600px; text-align: center;">
         <img style="text-align: center;margin: 0 auto;" width="200px" src="https://trello-attachments.s3.amazonaws.com/5c88f9faae42653719f426cd/5c8ababca4e26c31d0023d76/f14c15fdfa72fd1c1fe6d5c086558c5f/logo-ut-roxo-fundo-transparente.png"/>
      ${body}
      </div>
      <div style="Margin:0px auto;max-width:600px"><table align="center" border="0" cellpadding="0" cellspacing="0" style="width:100%"><tbody><tr><td style="direction:ltr;font-size:0px;padding:20px 0;padding-left:40px;padding-right:40px;padding-top:30px;text-align:center;vertical-align:top"><div class="m_8503563515105997816m_-3063299165141067094mj-column-per-100 m_8503563515105997816m_-3063299165141067094outlook-group-fix" style="font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%"><table border="0" cellpadding="0" cellspacing="0" style="vertical-align:top" width="100%"><tbody><tr><td align="center" style="font-size:0px;padding:10px 25px;word-break:break-word"><table align="center" border="0" cellpadding="0" cellspacing="0" style="float:none;display:inline-table"><tbody><tr><td style="padding:4px"><table border="0" cellpadding="0" cellspacing="0" style="border-radius:3px;width:40px"><tbody><tr><td style="font-size:0;height:40px;vertical-align:middle;width:40px"><a href="https://www.facebook.com/universotraducoes" target="_blank" ><img height="40" src="https://ci3.googleusercontent.com/proxy/svl2CAy--y9K0gRfH6gKHfZdhJ4CwmCwzTpRcWma1IJN9AtgKMDNO3wbhCbXc-61HWuL3FJBxdCjRfRMxqRKXo3b_pVmD4mPJFV-=s0-d-e1-ft#https://nu-emails.s3.amazonaws.com/ico-facebook-grey.png" style="border-radius:3px" width="40" class="CToWUd"></a></td></tr></tbody></table></td></tr></tbody></table><table align="center" border="0" cellpadding="0" cellspacing="0" style="float:none;display:inline-table"><tbody><tr><td style="padding:4px"><table border="0" cellpadding="0" cellspacing="0" style="border-radius:3px;width:40px"><tbody><tr><td style="font-size:0;height:40px;vertical-align:middle;width:40px"><a href="http://instagram.com/universotraducoes" target="_blank"><img height="40" src="https://ci6.googleusercontent.com/proxy/i1kT8xii1ssSuDBgsn9nnsHy_KLHNzEhU3Cn5ZzD53cx-wytJjsvmrS5EkOe-LESF2G56ANzpjB2-W9r8iLT2YIvZtbvRC1_abcBrQ=s0-d-e1-ft#https://nu-emails.s3.amazonaws.com/ico-instagram-grey.png" style="border-radius:3px" width="40" class="CToWUd"></a></td></tr></tbody></table></td></tr></tbody></table><table align="center" border="0" cellpadding="0" cellspacing="0" style="float:none;display:inline-table"><tbody><tr><td style="padding:4px"><table border="0" cellpadding="0" cellspacing="0" style="border-radius:3px;width:40px"><tbody><tr><td style="font-size:0;height:40px;vertical-align:middle;width:40px"><a href="https://linkedin.com/company/10123923" target="_blank" data-><img height="40" src="https://ci4.googleusercontent.com/proxy/NBrR1NWriKsqV1L1NMwyBId_TwJOHci-kOcrycokMT9iKg5qiTAemtSbOJ4IIef56VxPm1GmH3zAVV9YR_Rfle3RfzrNsV0T6MPa=s0-d-e1-ft#https://nu-emails.s3.amazonaws.com/ico-linkedin-grey.png" style="border-radius:3px" width="40" class="CToWUd"></a></td></tr></tbody></table></td></tr></tbody></table></td></tr></tbody></table></div></td></tr></tbody></table></div>
      <div style="font-family:sans-serif;font-size:13px;line-height:150%;text-align:center;color:#777777">Em caso de qualquer dúvida, fique à vontade para responder esse email ou nos contatar no <a href="mailto:contact@universotraducoes.com" style="color:#8c43c5;font-weight:bold" target="_blank">contact@universotraducoes.com</a>.<p>Obrigado!</p><p>
      Universo Traduçoes
      </p></div>
    </div>
    </body>
`

  const mailOptions = {
    from: 'Universo Traduções <contact@universotraducoes.com>', // sender address
    bcc: to, // list of receivers
    subject: subject, // Subject line
    html: mail // plain text body
  }

  var transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
      user: 'contact@universotraducoes.com',
      pass: 'uruguai32!'
    }
  })

  transporter.sendMail(mailOptions, function (err, info) {
    if (err) { console.log(err) } else { console.log(info) }
  })
}
