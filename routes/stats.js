var express = require('express')
var router = express.Router()
var User = require('../models/user')
var _ = require('lodash')
var moment = require('moment')
var format = require('../utils/format').format
var pagarme = require('pagarme')
var Translation = require('../models/translation')
var _ = require('lodash')
var Language = require('../models/language')

/* GET users listing. */
router.get('/users', function (req, res, next) {
  User.find({deleted: false}).populate('languages')
    .then(async users => {
      var startOfThisMonth = moment().startOf('month')
      var endOfThisMonth = moment().endOf('month')
      var statsUser = {}
      var totalTradutors = _.findIndex(users, 'is_tradutor')

      statsUser.total = users.length
      statsUser.total_tradutors = totalTradutors
      statsUser.total_users = users.length - totalTradutors

      var registeredThisMonth = 0
      _.each(users, function (o) {
        if (moment(o.created_at).isBetween(startOfThisMonth, endOfThisMonth)) {
          registeredThisMonth++
        }
      })
      statsUser.total_thisMonth = registeredThisMonth
      res.send(statsUser)
    })
})

router.get('/receita', async (req, res) => {
  let client
  try {
    client = await pagarme.client.connect({
      api_key: process.env.PAGARME || 'ak_test_alEqoLOSSF7cTrl929fYbCzV0TbzH8'
    })
  } catch (err) {
    console.log('Authentication error')
  }
  try {
    let receita = await client.balance.primary()
    console.log(receita)
    receita.total = format((receita.waiting_funds.amount + receita.available.amount + receita.transferred.amount))
    receita.waiting_funds.amount = format(receita.waiting_funds.amount)
    receita.available.amount = format(receita.available.amount)
    receita.transferred.amount = format(receita.transferred.amount)
    res.json(receita)
  } catch (err) {
    console.log(err)
  }
})

router.get('/translations', async (req, res) => {
  let translations = await Translation.find({})
  let inProgress = await Translation.find({status: 'Em Progresso'})
  res.json({total: translations.length, inProgress: inProgress.length})
})

router.get('/translations/month-graph', async (req, res) => {
  let translations = await Translation.find({})
  let months = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
  translations.forEach(translation => {
    console.log(translation.created_at)
    if (moment(translation.created_at).isAfter(moment('01/01/2019').format()) && moment(translation.created_at).isBefore(moment('01/01/2019').endOf('month').format())) {
      months[0]++
    } else if (moment(translation.created_at).isAfter(moment('02/01/2019').format()) && moment(translation.created_at).isBefore(moment('02/01/2019').endOf('month').format())) {
      months[1]++
    } else if (moment(translation.created_at).isAfter(moment('03/01/2019').format()) && moment(translation.created_at).isBefore(moment('03/01/2019').endOf('month').format())) {
      months[2]++
    } else if (moment(translation.created_at).isAfter(moment('04/01/2019').format()) && moment(translation.created_at).isBefore(moment('04/01/2019').endOf('month').format())) {
      months[3]++
    } else if (moment(translation.created_at).isAfter(moment('05/01/2019').format()) && moment(translation.created_at).isBefore(moment('05/01/2019').endOf('month').format())) {
      months[4]++
    } else if (moment(translation.created_at).isAfter(moment('06/01/2019').format()) && moment(translation.created_at).isBefore(moment('06/01/2019').endOf('month').format())) {
      months[5]++
    } else if (moment(translation.created_at).isAfter(moment('07/01/2019').format()) && moment(translation.created_at).isBefore(moment('07/01/2019').endOf('month').format())) {
      months[6]++
    } else if (moment(translation.created_at).isAfter(moment('08/01/2019').format()) && moment(translation.created_at).isBefore(moment('08/01/2019').endOf('month').format())) {
      months[7]++
    } else if (moment(translation.created_at).isAfter(moment('09/01/2019').format()) && moment(translation.created_at).isBefore(moment('09/01/2019').endOf('month').format())) {
      months[8]++
    } else if (moment(translation.created_at).isAfter(moment('10/01/2019').format()) && moment(translation.created_at).isBefore(moment('10/01/2019').endOf('month').format())) {
      months[9]++
    } else if (moment(translation.created_at).isAfter(moment('11/01/2019').format()) && moment(translation.created_at).isBefore(moment('11/01/2019').endOf('month').format())) {
      months[10]++
    } else if (moment(translation.created_at).isAfter(moment('12/01/2019').format()) && moment(translation.created_at).isBefore(moment('12/01/2019').endOf('month').format())) {
      months[11]++
    }
  })
  res.json(months)
})

router.get('/translations/pie-language', async (req, res) => {
  let languages = await Language.find({})
  let translations = await Translation.find({}).populate({
    path: 'serviceconfig',
    populate: {
      path: 'lang_dest',
      model: 'Language'
    }
  })
  let langArray = []
  let valueArray = []
  languages.forEach((language, index) => {
    langArray[index] = language.name
    valueArray[index] = 0
    translations.forEach(translation => {
      if (translation.serviceconfig.lang_dest[0].name === language.name) {
        valueArray[index]++
      }
    })
  })
  res.json({linguas: langArray, valores: valueArray})
})

router.get('/translations/pie-status', async (req, res) => {

})

module.exports = router
