var express = require('express')
var router = express.Router()
var Auth = require('../middlewares/auth')
var Translation = require('../models/translation')
var Serviceconfig = require('../models/serviceconfig')
var Comment = require('../models/comment')
var moment = require('moment')
var _ = require('lodash')
var multer = require('multer')
var User = require('../models/user')
var pagarme = require('pagarme')
var sendMail = require('../utils/sendMail')
var format = require('../utils/format').format2
const {google} = require('googleapis')
const nfe = require('../utils/nfe')

const oauth2Client = new google.auth.OAuth2(
  '912340549149-1nqt06nqdko4f5dmuvsmpame5q5ec33e.apps.googleusercontent.com',
  '8zEidC7n2B55dEmOWHHKvd5X',
  'https://plataforma.universotraducoes.com/translations/googlelogin'
)

const scopes = [
  'https://www.googleapis.com/auth/plus.me',
  'https://www.googleapis.com/auth/userinfo.profile',
  'https://www.googleapis.com/auth/userinfo.email'
]

const usersByTranslation = async (translation) => {
  let tradutors = await User.find({is_tradutor: true})
  console.log(tradutors.length)
  let langArr = [translation.serviceconfig.lang_dest[0]._id.toString(), translation.serviceconfig.lang_origem[0]._id.toString()]
  let users = tradutors.filter(tradutor => {
    let tradLangs = tradutor.languages.map(lang => lang.toString())
    if (_.intersection(tradLangs, langArr).length >= 2) {
      return tradutor.email
    }
  })
  let oi = users.map(user => user.email)
  oi.push('igorpollo@gmail.com')
  return oi
}

var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, 'uploads/')
  },
  filename: function (req, file, cb) {
    cb(null, Date.now() + '-' + file.originalname)
  }
})

var upload = multer({ storage: storage })
var uploadMany = upload.array('documents', 12)

router.get('/teste', async (req, res) => {
  let trad = await Translation.findOne({uid: 'UT-51241'}).populate('serviceconfig')
  usersByTranslation(trad)
})

router.get('/', Auth, function (req, res) {
  if (!req.user.is_tradutor) {
    console.log('teste')
    Translation.find({})
      .where('user').equals(req.user._id)
      .populate('user')
      .populate('tradutor')
      .populate({
        path: 'serviceconfig',
        populate: {
          path: 'lang_origem',
          model: 'Language'
        }
      })
      .populate({
        path: 'serviceconfig',
        populate: {
          path: 'lang_dest',
          model: 'Language'
        }
      })
      .sort({_id: -1})
      .then(translations => {
        _.each(translations, function (o) {
          o.deadline = moment(o.deadline).format('DD/MM/YYYY')
          o.created_at = moment(o.created_at).format('DD/MM/YYYY')
        })
        res.render('Translations/index.html', {translations: translations, current_user: req.user})
      })
      .catch(err => {
        res.status(500).send(err)
      })
  } else {
    console.log('teste')
    if (req.user.is_premium) {
      Translation.find({})
        .or([{ status: 'Aguardando Tradutor' }, { tradutor: req.user._id }])
        .populate('user')
        .populate('tradutor')
        .populate({
          path: 'serviceconfig',
          populate: {
            path: 'lang_origem',
            model: 'Language'
          }
        })
        .populate({
          path: 'serviceconfig',
          populate: {
            path: 'lang_dest',
            model: 'Language'
          }
        })
        .sort({_id: 1})
        .then(translations => {
          var userLanguagesArr = []

          _.each(req.user.languages, function (o) {
            userLanguagesArr.push(o._id.toString())
          })

          var newTranslationsArr = []
          var langsArr = []
          _.each(translations, function (o) {
            if (!o.serviceconfig) return
            langsArr = [o.serviceconfig.lang_dest[0]._id.toString(), o.serviceconfig.lang_origem[0]._id.toString()]
            let intersectionLength = 1
            o.serviceconfig.tipo_de_servico === 'Revisao' ? intersectionLength = 1 : intersectionLength = 2
            if (_.intersection(userLanguagesArr, langsArr).length >= intersectionLength) {
              newTranslationsArr.push(o)
            }
          })
          _.each(newTranslationsArr, function (o) {
            o.deadline = moment(o.deadline).format('DD/MM/YYYY')
            o.created_at = moment(o.created_at).format('DD/MM/YYYY')
            o.price = _.replace(o.price, '.', '')
            o.price = _.replace(o.price, ',', '.')
            o.price = ((o.price / 100) * 50).toFixed(2)
            o.price = format(o.price)
          })
          res.render('Translations/index.html', {translations: newTranslationsArr.reverse(), current_user: req.user})
        })
        .catch(err => {
          res.status(500).send(err)
        })
    } else {
      Translation.find({})
        .or([{ status: 'Aguardando Tradutor' }, { tradutor: req.user._id }])
        .where('public').equals(true)
        .populate('user')
        .populate('tradutor')
        .populate({
          path: 'serviceconfig',
          populate: {
            path: 'lang_origem',
            model: 'Language'
          }
        })
        .populate({
          path: 'serviceconfig',
          populate: {
            path: 'lang_dest',
            model: 'Language'
          }
        })
        .sort({_id: 1})
        .then(translations => {
          var userLanguagesArr = []
          console.log('REQQQQQQQQQQQQ')
          console.log(req.user.languages)
          _.each(req.user.languages, function (o) {
            userLanguagesArr.push(o._id.toString())
          })
          var newTranslationsArr = []
          var langsArr = []
          _.each(translations, function (o) {
            langsArr = [o.serviceconfig.lang_dest[0]._id.toString(), o.serviceconfig.lang_origem[0]._id.toString()]
            console.log(_.intersection(userLanguagesArr, langsArr))
            if (_.intersection(userLanguagesArr, langsArr).length >= 2) {
              newTranslationsArr.push(o)
            }
          })
          _.each(newTranslationsArr, function (o) {
            o.deadline = moment(o.deadline).format('DD/MM/YYYY')
            o.created_at = moment(o.created_at).format('DD/MM/YYYY')
            o.price = _.replace(o.price, ',', '.')
            o.price = ((o.price / 100) * 50).toFixed(2)
          })
          let contaBancaria = !!req.user.recebedor_id
          res.render('Translations/index.html', {translations: newTranslationsArr, current_user: req.user, contaBancaria})
        })
        .catch(err => {
          res.status(500).send(err)
        })
    }
  }
})

router.get('/accept/:id', Auth, async (req, res, next) => {
  let client
  let translation = await Translation.findOne({_id: req.params.id}).populate('user')
  try {
    client = await pagarme.client.connect({
      api_key: process.env.PAGARME || 'ak_test_alEqoLOSSF7cTrl929fYbCzV0TbzH8'
    })
  } catch (err) {
    console.log('Authentication error')
  }
  let urecebedor
  if (process.env.NODE_ENV === 'production') {
    urecebedor = 're_cjthtwiud1b5smi60aq3gvfyp'
  } else {
    urecebedor = 're_cjqp5ef24001cum6ew4v3s4lw'
  }
  console.log('KEYSSSSS')
  console.log(urecebedor)
  console.log(req.user.recebedor_id)
  try {
    let transaction = await client.transactions.capture({
      id: translation.transaction.id,
      split_rules: [
        {
          'recipient_id': 're_ck445aw2u0cuh9462nt1efar4',
          'liable': 'false',
          'charge_processing_fee': 'false',
          'percentage': '10'
        },
        {
          'recipient_id': urecebedor,
          'liable': 'true',
          'charge_processing_fee': 'true',
          'percentage': '40'
        },
        {
          'recipient_id': req.user.recebedor_id,
          'liable': 'false',
          'charge_processing_fee': 'false',
          'percentage': '50'
        }
      ]
    })
    if (transaction.status === 'paid' || translation.status == 'authorized') {
      let user = translation.user
      await Translation.updateOne({_id: translation._id}, {tradutor: req.user._id, status: 'Em Progresso'})
      sendMail(translation.user.email, 'Tradução Iniciada', '<h3>Olá, ' + translation.user.name + '</h3><p>Informamos que um tradutor iniciou o trabalho na sua tradução. Informaremos quando ela estiver concluída.</p>')
      nfe(user.name, user.email, translation.price)
      res.redirect('/translations/show/' + req.params.id)
    } else {
      res.redirect('/translations?paid=false')
    }
  } catch (err) {
    console.log(err.response.errors)
    req.session.flash = 'Erro ao processar o pagamento. Verifique seus dados bancários.'
    res.redirect('/translations')
  }
})

router.get('/thanks', Auth, function (req, res, next) {
  res.render('Translations/thanks.html', {current_user: req.user})
})

router.get('/concluir/:id', Auth, async (req, res, next) => {
  let translation = await Translation.findOne({_id: req.params.id}).populate('user').populate('tradutor')
  if (translation.revisao) {
    sendMail(translation.tradutor.email, 'Revisão Concluída', '<h3>Olá, ' + translation.tradutor.name + ' </h3><p>Informamos que a sua revisão <strong>' + translation.uid + '</strong> foi concluída</p>')
  } else {
    sendMail(translation.tradutor.email, 'Trabalho Concluído', '<h3>Olá, ' + translation.tradutor.name + ' </h3><p>Informamos que a sua tradução <strong>' + translation.uid + '</strong> foi concluída</p>')
  }
  await Translation.updateOne({_id: req.params.id}, {status: 'Concluído'})
  res.json('ok')
})

router.get('/cancelar/:id', Auth, async (req, res, next) => {
  req.session.flash = 'Tradução cancelada com sucesso!'
  await Translation.updateOne({_id: req.params.id}, {status: 'Cancelada'})
  res.redirect('/translations')
})

router.get('/revisao/:id', Auth, async (req, res, next) => {
  let translation = await Translation.findOne({_id: req.params.id}).populate('user').populate('tradutor')
  sendMail([translation.tradutor.email, 'atendimento@univerotraducoes.com'], 'Trabalho em Revisão', `<h3>Olá, ${translation.tradutor.name}</h3><p>Informamos que foi solicitado uma revisão para a tradução <strong>${translation.uid} </strong></p><p>Para visualiza-la,  <a href="https://plataforma.universotraducoes.com/translations/show/${translation._id}">Clique aqui</a></p>`)
  await Translation.updateOne({_id: req.params.id}, {status: 'Revisão', revisao: true})
  res.redirect('/translations/show/' + req.params.id + '?revisao=true')
})

router.get('/show/:id', Auth, function (req, res, next) {
  let revisao = req.query.revisao
  Translation.findOne({_id: req.params.id})
    .populate('user')
    .populate('tradutor')
    .populate({
      path: 'serviceconfig',
      populate: {
        path: 'lang_origem',
        model: 'Language'
      }
    })
    .populate({
      path: 'serviceconfig',
      populate: {
        path: 'lang_dest',
        model: 'Language'
      }
    })
    .then(translation => {
      translation.deadline = moment(translation.deadline).format('DD-MM-YYYY')
      if (!translation) {
        res.redirect('/translations')
      } else {
        if (req.user.is_tradutor) {
          translation.price = _.replace(translation.price, '.', '')
          translation.price = _.replace(translation.price, ',', '.')
          translation.price = ((translation.price / 100) * 50).toFixed(2)
          translation.price = format(translation.price)
        }
        if (translation.user._id.toString() === req.user._id.toString() || translation.status === 'Aguardando Tradutor' || translation.tradutor._id.toString() === req.user._id.toString() || req.user.is_admin) {
          console.log('caiu aqui')
          res.render('Translations/show.html', {translation: translation, current_user: req.user, revisao})
        } else {
          console.log('caiu aqui')
          res.redirect('/translations')
        }
      }
    })
})

router.post('/show/:id', Auth, function (req, res) {
  uploadMany(req, res, function (err) {
    if (err instanceof multer.MulterError) {
      req.session.flash = 'Número máximo de arquivos é 12'
      return res.redirect('/translations/show/' + req.params.id)
    } else {
      console.log(req.files)
      if (req.files === undefined || req.files.length < 1) {
        Translation.updateOne({_id: req.params.id},
          {
            translated_text: req.body.texto,
            status: 'Aguardando Aprovação',
            completed_at: moment().format()
          })
          .then(async translation => {
            let translation2 = await Translation.findOne({_id: req.params.id}).populate('user').populate('tradutor')
            sendMail(translation2.user.email, 'Trabalho Concluído', '<h3>Olá, ' + translation2.user.name + ' </h3><p>Informamos que a sua tradução <strong>' + translation2.uid + '</strong> foi concluída e aguarda sua aprovação</p><p><a href="https://plataforma.universotraducoes.com/translations">Clique aqui</a> para ir para o painel.</p>')
            return res.redirect('/translations')
          })
      } else {
        console.log('arquivo')
        let files = []
        for (let i = 0; i < req.files.length; i++) {
          files.push(req.files[i].filename)
        }
        Translation.updateOne({_id: req.params.id},
          {
            translated_files: files,
            status: 'Aguardando Aprovação',
            completed_at: moment().format()
          })
          .then(async translation => {
            let translation2 = await Translation.findOne({_id: req.params.id}).populate('user').populate('tradutor')
            console.log(translation2)
            sendMail(translation2.user.email, 'Trabalho Concluído', '<h3>Olá, ' + translation2.user.name + ' </h3><p>Informamos que a sua tradução <strong>' + translation2.uid + '</strong> foi concluída e aguarda sua aprovação</p><p><a href="https://plataforma.universotraducoes.com/translations">Clique aqui</a> para ir para o painel.</p>')
            return res.redirect('/translations')
          })
      }
    }
  })
})

router.post('/avaliar', async (req, res) => {
  console.log(req.body)
  await Translation.findOneAndUpdate({_id: req.body.id}, {
    avaliacao: true,
    avaliacao_qualidade: req.body.qualidade,
    avaliacao_preco: req.body.preco,
    avaliacao_prazo: req.body.prazo,
    avaliacao_usabilidade: req.body.usabilidade,
    avaliacao_flexibilidade: req.body.flexibilidade,
    avaliacao_pagamento: req.body.pagamento
  })
  res.redirect('/translations')
})

router.post('/serviceconfig/find', function (req, res) {
  console.log(req.body)
  if (req.body.qualidade) {
    Serviceconfig.findOne()
      .where('qualidade').equals(req.body.qualidade)
      .where('lang_origem').equals(req.body.lang_origem)
      .where('lang_dest').equals(req.body.lang_dest)
      .where('tipo_de_servico').equals(req.body.tipo_de_servico)
      .then(response => {
        res.json(response)
      })
  } else {
    Serviceconfig.find({deleted: false})
      .where('lang_origem').equals(req.body.lang_origem)
      .where('lang_dest').equals(req.body.lang_dest)
      .where('tipo_de_servico').equals(req.body.tipo_de_servico)
      .then(response => {
        const data = response.filter(lang => {
          return lang.qualidade !== 'Premium'
        })
        console.log(data)
        res.json(data)
      })
  }
})

router.post('/', upload.single('glossario'), function (req, res) {
  console.log(req.body)
  req.body.uid = 'UT-' + (Math.floor(Math.random() * 90000) + 10000)
  req.body.created_at = moment().format()
  if (req.file) {
    console.log(req.file)
    req.body.glossario = req.file.filename
  }
  let price = req.body.price
  price = price.replace(',', '')
  price = price.replace('.', '')
  if (price < 1000) {
    req.body.price = '10,00'
  }
  if (req.session.user) {
    req.body.user = req.session.user
    Translation.create(req.body)
      .then(translation => {
        res.redirect('/translations/pay?id=' + translation._id)
      })
      .catch(err => {
        res.status(500).send(err)
      })
  } else {
    Translation.create(req.body)
      .then(translation => {
        res.redirect('/translations/register?id=' + translation._id)
      })
      .catch(err => {
        res.status(500).send(err)
      })
  }
})

router.get('/googlelogin', async (req, res) => {
  let code = req.query.code
  let translation = req.query.state
  let session = req.session
  const {tokens} = await oauth2Client.getToken(code)
  oauth2Client.setCredentials(tokens)
  const plus = google.plus({
    version: 'v1',
    auth: oauth2Client
  })
  const user = await plus.people.get({userId: 'me'})
  let usuario = await User.findOne({email: user.data.emails[0].value})
  let image = user.data.image.url
  image = image.replace(/\?(.*)/, '')
  if (usuario) {
    await User.updateOne({email: usuario.email}, {
      picture: image,
      name: user.data.displayName,
      google_id: user.data.id
    })
    await Translation.updateOne({_id: translation}, {user: usuario._id})
    session.user = usuario._id
    res.redirect('/translations/pay?id=' + translation)
  } else {
    let newUser = await User.create({
      email: user.data.emails[0].value,
      picture: image,
      active: true,
      password: Math.random().toString(36).substring(7),
      name: user.data.displayName
    })
    sendMail(newUser.email, 'Bem vindo à Universo Traduções', '<h3>Bem vindo</h3><p>Estamos muito felizes por você ter se cadastrado</p>')
    await Translation.updateOne({_id: translation}, {user: newUser._id})
    session.user = newUser._id
    res.redirect('/translations/pay?id=' + translation)
  }
})

router.get('/register', function (req, res) {
  if (req.query.id) {
    const url = oauth2Client.generateAuthUrl({
      access_type: 'offline',
      scope: scopes,
      state: req.query.id
    })
    Translation.findOne({_id: req.query.id})
      .populate('serviceconfig')
      .then(translation => {
        translation.deadline = moment(translation.deadline).format('DD/MM/YYYY')
        res.render('Translations/register.html', {translation: translation, url})
      })
  } else {
    res.render('Translations/register.html')
  }
})

router.get('/pay', Auth, function (req, res) {
  if (req.query.id) {
    Translation.findOne({_id: req.query.id})
      .populate('serviceconfig')
      .then(translation => {
        translation.deadline = moment(translation.deadline).format('DD/MM/YYYY')
        res.render('Translations/pay.html', {translation: translation, current_user: req.user})
      })
  } else {
    res.render('Translations/register.html')
  }
})

router.post('/pay', Auth, async (req, res) => {
  let translation = await Translation.findOne({_id: req.body.translationID}).populate('serviceconfig')
  let price = translation.price.replace(',', '')
  price = price.replace('.', '')
  console.log(process.env.PAGARME)
  let client
  try {
    client = await pagarme.client.connect({
      api_key: process.env.PAGARME || 'ak_test_alEqoLOSSF7cTrl929fYbCzV0TbzH8'
    })
  } catch (err) {
    console.log('Authentication error')
  }

  let card
  let noSlashes = req.body.card_expiration.replace(/\//g, '')
  try {
    card = await client.cards.create({
      card_number: req.body.cartao,
      card_holder_name: req.body.nome,
      card_expiration_date: noSlashes,
      card_cvv: req.body.x_card_code
    })
  } catch (err) {
    console.log(err)
  }

  let transaction
  let transactionPayload = {
    amount: price,
    card_id: card.id,
    payment_method: 'credit_card',
    capture: false
  }
  try {
    transaction = await client.transactions.create(transactionPayload)
  } catch (err) {
    console.log(err)
  }
  let status
  let refuseReason = ''
  try {
    await Translation.updateOne({_id: translation._id}, {
      'transaction.status': transaction.status,
      'transaction.refuse_reason': refuseReason,
      'transaction.date_created': transaction.date_created,
      'transaction.id': transaction.id,
      'transaction.card_brand': transaction.card_brand,
      'transaction.card_last_digits': transaction.card_last_digits,
      'transaction.payment_method': transaction.payment_method
    })
    req.session.flash = `payment:${transaction.status}`
  } catch (err) {
    console.log(err)
  }
  if (transaction.status === 'refused' || transaction.status === 'processing') {
    status = transaction.status
    await Translation.updateOne({_id: translation._id}, {status})
    refuseReason = transaction.refuse_reason
    sendMail(req.user.email, 'Não foi possível processar seu pagamento', '<h3>Pagamento recusado</h3><p>Informamos que seu pagamento não efetuado. Você pode tentar fazer o pagamento novamente com outro cartão.</p>')
    res.redirect('/translations')
  } else {
    let tradutors = await usersByTranslation(translation)
    sendMail(tradutors, 'Nova Tradução', '<h3>Nova Tradução Disponível</h3><p>Olá, Informamos que uma nova tradução compatível com seu perfil está disponível.</p><p>Vá até o site para verificar o projeto: <a href="https://plataforma.universotraducoes.com/translations">Ir para a plataforma</a></p>')
    sendMail('nschultz@universotraducoes.com', 'Nova Tradução', '<h3>Nova Tradução Cadastrada</h3><p>Olá, Informamos que uma nova tradução foi solicitada no sistema. Veja no <a href="https://plataforma.universotraducoes.com/admin">Admin</a></p></a></p>')
    sendMail(req.user.email, 'Pagamento Aprovado', '<h3>Pagamento Aprovado</h3><p>Informamos que seu pagamento foi aprovado. Assim que um tradutor começar a sua tradução você será notificado.</p>')
    status = 'Aguardando Tradutor'
    await Translation.updateOne({_id: translation._id}, {status})
    res.redirect('/translations/thanks')
  }

})

router.post('/payboleto', Auth, async (req, res) => {
  let translation = await Translation.findOne({_id: req.body.translationID}).populate('serviceconfig')
  let price = translation.price.replace(',', '')
  price = price.replace('.', '')
  let client
  try {
    client = await pagarme.client.connect({
      api_key: process.env.PAGARME || 'ak_test_alEqoLOSSF7cTrl929fYbCzV0TbzH8'
    })
  } catch (err) {
    console.log('Authentication error')
  }

  let transaction
  let transactionPayload = {
    amount: price,
    payment_method: 'boleto',
    capture: true,
    customer: {
      'external_id': translation.uid,
      'name': req.user.name,
      'type': 'individual',
      'country': 'br',
      'email': req.user.email,
      'documents': [
        {
          'type': 'cpf',
          'number': '30621143049'
        }
      ],
      'phone_numbers': ['+5511999998888'],
      'birthday': '1965-01-01'
    }
  }
  try {
    transaction = await client.transactions.create(transactionPayload)
  } catch (err) {
    console.log(err)
  }
  console.log(transaction)
  let status
  let refuseReason = ''
  if (transaction.status === 'refused') {
    status = transaction.status
    refuseReason = transaction.refuse_reason
    sendMail(req.user.email, 'Não foi possível emitir seu boleto', '<h3>Erro ao emitir boleto</h3><p>Informamos que seu pagamento não foi efetuado. Você pode tentar fazer o pagamento novamente com outro cartão.</p>')
  } else {
    sendMail(req.user.email, 'Boleto gerado', `<h3>Aguardando Pagamento</h3><p>Para imprimir ou visualizar seu boleto <a href="${transaction.boleto_url}">Clique aqui</a></a></p>`)
    status = 'Aguardando Pagamento'
  }
  try {
    await Translation.updateOne({_id: translation._id}, {
      status: status,
      boleto_url: transaction.boleto_url,
      'transaction.status': transaction.status || '',
      'transaction.refuse_reason': refuseReason || '',
      'transaction.date_created': transaction.date_created || '',
      'transaction.id': transaction.id || '',
      'transaction.card_brand': transaction.card_brand || '',
      'transaction.card_last_digits': transaction.card_last_digits || '',
      'transaction.payment_method': transaction.payment_method || ''
    })
    res.redirect('/translations')
  } catch (err) {
    console.log(err)
  }
})

router.post('/login', async (req, res) => {
  User.findOne()
    .where('email').equals(req.body.email)
    .where('password').equals(req.body.password)
    .then(async user => {
      if (user) {
        var session = req.session
        session.user = user._id
        await Translation.findOneAndUpdate({_id: req.body.translationID}, {user: user._id})
        res.redirect('/translations/pay?id=' + req.body.translationID)
      } else {
        res.redirect('/translation/register?attempt=true&id=' + req.body.translationID)
      }
    })
})

router.post('/register', async (req, res) => {
  User.create(req.body)
    .then(async user => {
      var session = req.session
      session.user = user._id
      session.page = 'https://plataforma.universotraducoes.com/translations/pay?id=' + req.body.translationID
      await Translation.findOneAndUpdate({_id: req.body.translationID}, {user: user._id})
      res.redirect('/translations/pay?id=' + req.body.translationID)
    })
    .catch(err => {
      console.log(err)
    })
})

router.get('/new', function (req, res) {
  if (req.session.user) {
    User.findOne({ _id: req.session.user }, function (err, user) {
      if (err) { res.send(err) }
      req.user = user
    }).then(user => {
      res.render('Translations/new.html', {current_user: user})
    })
  } else {
    res.render('Translations/new.html', {current_user: ''})
  }
})

router.get('/comments/:id', Auth, function (req, res) {
  Comment
    .find({translation: req.params.id})
    .populate('user')
    .then(comments => {
      comments = comments.map(comment => {
        if (comment.time) {
          comment.time = moment(parseInt(comment.time)).format('LLL')
        }
        return comment
      })
      res.json(comments)
    })
})
router.post('/comment', Auth, async (req, res) => {
  req.body.user = req.user._id
  let translation = await Translation.findOne({_id: req.body.translation}).populate('user').populate('tradutor')
  if (translation.user._id == req.user._id) {
    sendMail([translation.tradutor.email, 'atendimento@universotraducoes.com'], 'Novo Comentário', `<h3>Olá, ${translation.tradutor.name}</h3><p> Um novo comentário foi adiconado à traduçao ${translation.uid}.</p><p>Para visualizar <a href="https://plataforma.universotraducoes.com/translations/show/${translation._id}">clique aqui</a></p>`)
  } else {
    sendMail([translation.user.email, 'atendimento@universotraducoes.com'], 'Novo Comentário', `<h3>Olá, ${translation.user.name}</h3><p> Um novo comentário foi adiconado à traduçao ${translation.uid}.</p><p>Para visualizar <a href="https://plataforma.universotraducoes.com/translations/show/${translation._id}">clique aqui</a></p>`)
  }
  req.body.time = new Date().getTime()
  Comment.create(req.body)
    .then(comment => {
      res.json({status: 'success', data: comment})
    })
    .catch(err => { res.json(err) })
})

module.exports = router
