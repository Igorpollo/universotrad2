var express = require('express')
var router = express.Router()
var Language = require('../models/language')
var Serviceconfig = require('../models/serviceconfig')
var Admin = require('../middlewares/admin')
var User = require('../models/user')
var Ticket = require('../models/ticket')
var _ = require('lodash')
var moment = require('moment')
var format = require('../utils/format').format
var Translation = require('../models/translation')
var pagarme = require('pagarme')
var sendMail = require('../utils/sendMail')
/* GET users listing. */
router.get('/', Admin, function (req, res, next) {
  res.render('Admin/dashboard.html', {user: req.user})
})

// LANGUAGE START
router.get('/languages', Admin, function (req, res) {
  var message = false
  req.query.success ? message = true : message = false

  Language.find().where('deleted').ne(true).lean()
    .then(languages => {
      res.render('Admin/languages/index.html', {message: message, languages: languages, user: req.user})
    })
})

router.get('/translations', Admin, async (req, res) => {
  var message = false
  req.query.success ? message = true : message = false

  let translations = await Translation.find().where('deleted').ne(true)
    .populate('user')
    .populate('tradutor')
    .populate({
      path: 'serviceconfig',
      populate: {
        path: 'lang_origem',
        model: 'Language'
      }
    })
    .populate({
      path: 'serviceconfig',
      populate: {
        path: 'lang_dest',
        model: 'Language'
      }
    })
  _.each(translations, function (o) {
    o.deadline = moment(o.deadline).format('DD/MM/YYYY')
    o.created_at = moment(o.created_at).format('DD/MM/YYYY')
    o.completed_at = moment(o.completed_at).format('DD/MM/YYYY')
  })
  let all = translations.length
  res.render('Admin/translations/index.html', {message: message, translations: translations, user: req.user, all})
})

router.get('/languages/new', Admin, function (req, res) {
  res.render('Admin/languages/new.html', {user: req.user})
})

router.post('/languages/new', Admin, function (req, res) {
  Language.create(req.body)
    .then(response => {
      res.redirect('/admin/languages?success')
    })
    .catch(err => {
      res.status(500).send(err)
    })
})

router.get('/languages/edit/:id', Admin, function (req, res) {
  Language.findOne({_id: req.params.id})
    .then(language => {
      res.render('Admin/languages/edit.html', {language: language, user: req.user})
    })
    .catch(err => {
      res.status(500).send(err)
    })
})

router.get('/payments/:id', Admin, async (req, res, next) => {
  let client
  try {
    client = await pagarme.client.connect({
      api_key: process.env.PAGARME || 'ak_test_alEqoLOSSF7cTrl929fYbCzV0TbzH8'
    })
  } catch (err) {
    console.log(err)
  }

  let user = await User.findOne({_id: req.params.id})
  let saldo = await client.balance.find({ recipientId: user.recebedor_id })
  saldo.total = format((saldo.transferred.amount + saldo.available.amount + saldo.waiting_funds.amount))
  saldo.waiting_funds.amount = format(saldo.waiting_funds.amount)
  saldo.available.amount = format(saldo.available.amount)
  saldo.transferred.amount = format(saldo.transferred.amount)
  let translations = await Translation.find({}).or([{ tradutor: req.params.id }, { user: req.params.id }]).sort({_id: -1})
  if (req.user.is_tradutor) {
    _.each(translations, function (o) {
      o.deadline = moment(o.deadline).format('DD/MM/YYYY')
      o.price = _.replace(o.price, ',', '.')
      o.created_at = moment(o.created_at).format('DD/MM/YYYY')
      o.price = ((o.price / 100) * 50).toFixed(2)
    })
  }
  res.render('Payment/tradutor.html', {current_user: req.user, saldo, translations})
})

router.post('/languages/edit/:id', Admin, function (req, res) {
  Language.findByIdAndUpdate(req.params.id, req.body)
    .then(response => {
      res.redirect('/admin/languages?success=true')
    })
    .catch(err => {
      res.status(500).send(err)
    })
})

router.get('/languages/delete/:id', Admin, function (req, res) {
  Language.deleteOne({_id: req.params.id})
    .then(response => {
      res.redirect('/admin/languages?success=true')
    })
    .catch(err => {
      res.json(err)
    })
})

// LANGUAGE END

// SERVICECONFIG START

router.get('/serviceconfigs', Admin, function (req, res) {
  var message = false
  req.query.success ? message = true : message = false

  Serviceconfig.find().where('deleted').ne(true).populate('lang_origem').populate('lang_dest')
    .then(serviceconfigs => {
      console.log(serviceconfigs)
      res.render('Admin/serviceconfigs/index.html', {message: message, serviceconfigs: serviceconfigs, user: req.user})
    })
})

router.get('/serviceconfigs/new', Admin, function (req, res) {
  Language.find().where('deleted').ne(true).lean()
    .then(languages => {
      res.render('Admin/serviceconfigs/new.html', {languages: languages, user: req.user})
    })
})

router.post('/serviceconfigs/new', Admin, function (req, res) {
  Serviceconfig.create(req.body)
    .then(response => {
      res.redirect('/admin/serviceconfigs?success')
    })
    .catch(err => {
      res.status(500).send(err)
    })
})

router.get('/serviceconfigs/edit/:id', Admin, function (req, res) {
  Serviceconfig.findOne({_id: req.params.id})
    .then(async serviceconfig => {
      let languages = await Language.find().where('deleted').ne(true).lean()
      res.render('Admin/serviceconfigs/edit.html', {serviceconfig: serviceconfig, user: req.user, languages: languages})
    })
    .catch(err => {
      res.status(500).send(err)
    })
})

router.post('/serviceconfigs/edit/:id', Admin, function (req, res) {
  Serviceconfig.findByIdAndUpdate(req.params.id, req.body)
    .then(response => {
      res.redirect('/admin/serviceconfigs?success=true')
    })
    .catch(err => {
      res.status(500).send(err)
    })
})

router.get('/serviceconfigs/delete/:id', Admin, function (req, res) {
  Serviceconfig.deleteOne({_id: req.params.id})
    .then(response => {
      res.redirect('/admin/serviceconfigs?success=true')
    })
    .catch(err => {
      res.json(err)
    })
})

//  USERS START

router.get('/usuarios', Admin, function (req, res) {
  var message = false
  req.query.success ? message = true : message = false

  User.find().where('deleted').ne(true).populate('languages')
    .then(async users => {
      var all = users.length
      res.render('Admin/users/index.html', {message: message, users: users, user: req.user, all: all})
    })
})

router.get('/usuarios/json', function (req, res) {
  User.find()
    .where('is_tradutor').ne(true)
    .where('deleted').ne(true)
    .sort({name: 1})
    .populate('languages')
    .then(async users => {
      console.log(users)
      res.send(users)
    })
})

router.get('/tradutors', Admin, async (req, res) => {
  var message = false
  req.query.success ? message = true : message = false

  let client
  try {
    client = await pagarme.client.connect({
      api_key: process.env.PAGARME || 'ak_test_alEqoLOSSF7cTrl929fYbCzV0TbzH8'
    })
  } catch (err) {
    console.log(err)
  }

  User.find({is_tradutor: true}).populate('languages')
    .then(async users => {
      let all = users.length
      let tradutors = await Promise.all(users.map(async user => {
        let saldo = await client.balance.find({ recipientId: user.recebedor_id })
        saldo.total = format((saldo.transferred.amount + saldo.available.amount + saldo.waiting_funds.amount))
        saldo.waiting_funds.amount = format(saldo.waiting_funds.amount)
        saldo.available.amount = format(saldo.available.amount)
        saldo.transferred.amount = format(saldo.transferred.amount)
        user.balance = saldo
        return user
      }))
      res.render('Admin/tradutors/index.html', {message: message, tradutors, user: req.user, all: all})
    })
})

router.get('/tradutors/json', async (req, res) => {
  let client
  try {
    client = await pagarme.client.connect({
      api_key: process.env.PAGARME || 'ak_test_alEqoLOSSF7cTrl929fYbCzV0TbzH8'
    })
  } catch (err) {
    console.log(err)
  }

  User.find({is_tradutor: true})
    .where('deleted').ne(true)
    .sort({name: 1})
    .populate('languages')
    .then(async users => {
      let tradutors = await Promise.all(users.map(async user => {
        let saldo = await client.balance.find({ recipientId: user.recebedor_id })
        saldo.total = format((saldo.transferred.amount + saldo.available.amount + saldo.waiting_funds.amount))
        saldo.waiting_funds.amount = format(saldo.waiting_funds.amount)
        saldo.available.amount = format(saldo.available.amount)
        saldo.transferred.amount = format(saldo.transferred.amount)
        user.balance = saldo
        return user
      }))
      res.send(tradutors)
    })
})

router.get('/usuarios/edit/:id', Admin, function (req, res) {
  User.findOne({_id: req.params.id}).populate('languages')
    .then(async eduser => {
      let languages = await Language.find().where('deleted').ne(true).lean()
      let onlyIds = []
      let userLanguages = eduser.languages
      userLanguages.forEach(element => {
        onlyIds.push(element.name)
      })

      res.render('Admin/users/edit.html', {user: req.user, eduser: eduser, languages, onlyIds})
    })
    .catch(err => {
      res.status(500).send(err)
    })
})

router.get('/usuarios/new', Admin, async (req, res) => {
  let languages = await Language.find({}).lean()
  res.render('Admin/users/new.html', {user: req.user, languages})
})

router.post('/usuarios/new', Admin, async (req, res) => {
  let client
  try {
    client = await pagarme.client.connect({
      api_key: process.env.PAGARME || 'ak_test_alEqoLOSSF7cTrl929fYbCzV0TbzH8'
    })
  } catch (err) {
    console.log(err)
  }

  let bank
  try {
    bank = await client.bankAccounts.create({
      bank_code: req.body.banco,
      agencia: req.body.agencia.trim(),
      conta: req.body.conta.trim(),
      conta_dv: req.body.conta_dg.trim(),
      legal_name: req.body.nome_completo,
      document_number: req.body.cpf_cnpj
    })
  } catch (err) {
    console.log(err)
  }

  let recipient
  try {
    recipient = await client.recipients.create({
      bank_account_id: bank.id,
      transfer_interval: 'monthly',
      transfer_day: 5,
      transfer_enabled: true,
      postback_url: 'https://plataforma.universotraducoes.com/postback/recipient'
    })
  } catch (err) {
    console.log(err)
  }
  req.body.recebedor_id = recipient.id
  req.body.active = true
  let password = Math.random().toString(36).substring(7)
  req.body.password = password
  let user = await User.findOne({ email: req.body.email })
  if (user && user._id) {
    req.session.flash = 'Email já cadastrado'
    res.redirect('/admin/usuarios/new?success=false')
    return
  }
  await User.create(req.body)
  sendMail(req.body.email, 'Bem vindo Universo Traduções', '<h3>Olá, Bem vindo Universo</h3><p>Etamos felizes por você ter se cadastrado e se tornado nosso tradutor parceiro.</p><p>Sua senha de acesso é: <p><strong>' + password + '</strong></p>')
  res.redirect('/admin/usuarios/new?success=true')
})

router.post('/clientes/new/json', async (req, res) => {
  let user = await User.find({email: req.body.email})
  req.body.created_at = moment().format('DD/MM/YYYY')
  if (user.length > 0) {
    req.session.flash = 'Email já cadastrado'
    res.send('error')
  } else {
    req.body.active = true
    User.create(req.body)
      .then(user => {
        var session = req.session
        session.user = user._id
        // sendMail(user.email, `Bem vindo Universo das Traduções`, `<h3>Olá, ${user.name}</h3><p>Para confirmar o seu cadastro, clique no link: <a href="${req.protocol + '://' + req.get('host')}/activate/${activateCode}">Ativar minha conta</a></p>`)
        res.send('ok')
      })
      .catch(err => {
        console.log(err)
      })
  }
})

router.post('/clientes/list/json', async (req, res) => {
  let user = await User.find({email: req.body.email})
  req.body.created_at = moment().format('DD/MM/YYYY')
  if (user.length > 0) {
    req.session.flash = 'Email já cadastrado'
    res.send('error')
  } else {
    req.body.active = true
    User.create(req.body)
      .then(user => {
        var session = req.session
        session.user = user._id
        // sendMail(user.email, `Bem vindo Universo das Traduções`, `<h3>Olá, ${user.name}</h3><p>Para confirmar o seu cadastro, clique no link: <a href="${req.protocol + '://' + req.get('host')}/activate/${activateCode}">Ativar minha conta</a></p>`)
        res.send('ok')
      })
      .catch(err => {
        console.log(err)
      })
  }
})

router.post('/usuarios/new/json', async (req, res) => {
  console.log(req.body)
  let client
  try {
    client = await pagarme.client.connect({
      api_key: process.env.PAGARME || 'ak_test_alEqoLOSSF7cTrl929fYbCzV0TbzH8'
    })
  } catch (err) {
    console.log(err)
  }

  let bank
  try {
    bank = await client.bankAccounts.create({
      bank_code: req.body.banco,
      agencia: req.body.agencia.trim(),
      conta: req.body.conta.trim(),
      conta_dv: req.body.conta_dg.trim(),
      legal_name: req.body.nome_completo,
      document_number: req.body.cpf_cnpj
    })
  } catch (err) {
    console.log(err)
  }

  let recipient
  try {
    recipient = await client.recipients.create({
      bank_account_id: bank.id,
      transfer_interval: 'monthly',
      transfer_day: 5,
      transfer_enabled: true,
      postback_url: 'https://plataforma.universotraducoes.com/postback/recipient'
    })
  } catch (err) {
    console.log(err)
  }
  req.body.recebedor_id = recipient.id
  req.body.active = true
  let password = Math.random().toString(36).substring(7)
  req.body.password = password
  await User.create(req.body)
  // sendMail(req.body.email, 'Bem vindo Universo Traduções', '<h3>Olá, Bem vindo Universo</h3><p>Etamos felizes por você ter se cadastrado e se tornado nosso tradutor parceiro.</p><p>Sua senha de acesso é: <p><strong>' + password + '</strong></p>')
  res.send('ok')
})

router.post('/usuarios/edit/:id', Admin, function (req, res) {
  User.findByIdAndUpdate(req.params.id, req.body)
    .then(response => {
      res.redirect('/admin/usuarios?success=true')
    })
    .catch(err => {
      res.status(500).send(err)
    })
})

router.get('/usuarios/deletar/:id', Admin, function (req, res) {
  User.deleteOne({_id: req.params.id})
    .then(response => {
      res.redirect('/admin/usuarios')
    })
    .catch(err => {
      res.status(500).send(err)
    })
})

// TICKETS START

router.get('/tickets', Admin, function (req, res) {
  var message = false
  req.query.success ? message = true : message = false

  Ticket.find().where('deleted').ne(true).populate('user')
    .then(async tickets => {
      res.render('Admin/tickets/index.html', {message: message, tickets: tickets, user: req.user})
    })
})

router.get('/tickets/edit/:id', Admin, function (req, res) {
  Ticket.findOne({_id: req.params.id})
    .then(async ticket => {
      res.render('Admin/tickets/edit.html', {user: req.user, ticket: ticket})
    })
    .catch(err => {
      res.status(500).send(err)
    })
})

router.post('/tickets/edit/:id', Admin, async (req, res) => {
  let ticket = await Ticket.findOne({_id: req.params.id}).populate('user')
  Ticket.findByIdAndUpdate(req.params.id, req.body)
    .then(response => {
      sendMail(ticket.user.email, 'Você recebeu uma ressposta do seu Ticket', 'Você recebeu uma reposta sobre um ticket aberto. Para visualiza-la acesse <a href="https://plataforma.universotraducoes.com/tickets">Painel de Tickets</a>')
      res.redirect('/admin/tickets?success=true')
    })
    .catch(err => {
      res.status(500).send(err)
    })
})

/// ///////////////

router.post('/users/add', async (req, res) => {
  console.log(req.body)
  try {
    let user = await User.findOne({ email: req.body.email })
    if (user && user._id) {
      return res.send('Email ja existe')
    }
    await User.create(req.body)
    res.send('ok')
  } catch (err) {
    console.log(err)
    res.send(err)
  }
})

router.get('/users/find/:id', async (req, res) => {
  User.findOne({_id: req.params.id})
    .then(async client => {
      res.send(client)
    })
    .catch(err => {
      res.status(500).send(err)
    })
})

router.post('/users/edit/:id', async (req, res) => {
  console.log(req.params.id)
  console.log(req.body)
  await User.updateOne({_id: req.params.id}, req.body)
  res.send('ok')
})

router.post('/users/logintradutor', async (req, res) => {
  const user = await User.findOne({email: req.body.email})
  if (!user.id || !user.cpf) {
    return res.send({})
  }
  const recievedPassword = req.body.password
  const tradutorPassword = user.cpf.replace('.', '').slice(0, 4)
  if (recievedPassword === tradutorPassword) {
    res.send(user)
  } else {
    res.send({})
  }
})

module.exports = router
