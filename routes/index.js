var express = require('express')
var router = express.Router()
var User = require('../models/user')
var Auth = require('../middlewares/auth')
var Language = require('../models/language')
var PDFParser = require('pdf2json')
var count = require('word-count')
var WordExtractor = require('word-extractor')
var multer = require('multer')
var makeid = require('../utils/makeid')
var mammoth = require('mammoth')
var pagarme = require('pagarme')
var format = require('../utils/format').format
var Translation = require('../models/translation')
var axios = require('axios')
var sendMail = require('../utils/sendMail')
var _ = require('lodash')
var franc = require('franc')
var textract = require('textract')
const {google} = require('googleapis')
var moment = require('moment')
moment.locale('pt-br')
var nfe = require('../utils/nfe')

const oauth2Client = new google.auth.OAuth2(
  '912340549149-1nqt06nqdko4f5dmuvsmpame5q5ec33e.apps.googleusercontent.com',
  '8zEidC7n2B55dEmOWHHKvd5X',
  'https://plataforma.universotraducoes.com/googlelogin'
)

const scopes = [
  'https://www.googleapis.com/auth/plus.me',
  'https://www.googleapis.com/auth/userinfo.profile',
  'https://www.googleapis.com/auth/userinfo.email'
]

var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, 'uploads/')
  },
  filename: function (req, file, cb) {
    let newFile 
    if(file.mimetype == 'application/vnd.ms-excel') {
      newFile = file.originalname.replace(/(.xls)$/i, ".xlsx")
      file.mimetype = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
    } else if(file.mimetype == 'application/vnd.ms-powerpoint') {
      newFile = file.originalname.replace(/(.ppt)$/i, ".pptx")
      file.mimetype = 'application/vnd.openxmlformats-officedocument.presentationml.presentation'
    } else {
      newFile = file.originalname
    }
    cb(null, Date.now() + '-' + newFile)
  }
})

var upload = multer({ storage: storage })

/* GET home page. */
router.get('/', async (req, res, next) => {
  if (req.session.user) {
    let user = await User.findOne({ _id: req.session.user })
    if (user.is_tradutor) {
      res.redirect('/translations')
    } else {
      res.redirect('/translations/new')
    }
  }
  res.redirect('/translations/new')
})

router.get('/flash', function (req, res) {
  req.session.flash = `payment:authorized`
  res.redirect('/translations')
})
router.get('/registrar', function (req, res, next) {
  let url = oauth2Client.generateAuthUrl({
    access_type: 'offline',
    scope: scopes
  })
  res.render('Auth/register.html', {url})
})

router.post('/postback/recipient', function (req, res) {
  console.log(req.query)
  console.log(req.params)
  console.log(req.body)
  console.log('postback GET')
  res.json('ok')
})

router.get('/postback/recipient', function (req, res) {
  console.log(req.query)
  console.log(req.params)
  console.log(req.body)
  console.log('postback POST')
  res.json('ok')
})

router.get('/auth/facebook/callback', async (req, res) => {
  let session = req.session
  let translation = req.query.state
  let data = await axios.get('https://graph.facebook.com/v3.2/oauth/access_token?client_id=664147130649969&redirect_uri=https://plataforma.universotraducoes.com/auth/facebook/callback&client_secret=cf8ea0957cad3fdbb38d7b8b325042b1&code=' + req.query.code)
  let accessToken = data.data.access_token
  let getUser = await axios.get('https://graph.facebook.com/v3.2/me?access_token=' + accessToken + '&fields=id,name%2Cemail%2Cabout%2Cpicture%7Burl%7D&format=json&method=get&pretty=0')
  let fbUser = getUser.data
  let user = await User.findOne({email: fbUser.email})
  if (user) {
    await User.findOneAndUpdate({email: fbUser.email}, {
      facebook_id: fbUser.id,
      name: fbUser.name
    })
    console.log(user._id)
    session.user = user._id
    if (translation) {
      res.redirect('/translations/pay?id=' + translation)
    } else {
      res.redirect('/')
    }
  } else {
    let newUser = await User.create({
      name: fbUser.name,
      email: fbUser.email,
      active: true,
      picture: 'http://graph.facebook.com/' + fbUser.id + '/picture?type=large',
      facebook_id: fbUser.id
    })
    session.user = newUser._id
    if (translation) {
      res.redirect('/translations/pay?id=' + translation)
    } else {
      res.redirect('/')
    }
  }
})

router.get('/payments', Auth, async (req, res, next) => {
  let client
  try {
    client = await pagarme.client.connect({
      api_key: process.env.PAGARME || 'ak_test_alEqoLOSSF7cTrl929fYbCzV0TbzH8'
    })
  } catch (err) {
    console.log(err)
  }

  let saldo = await client.balance.find({ recipientId: req.user.recebedor_id })
  saldo.total = format((saldo.transferred.amount + saldo.available.amount + saldo.waiting_funds.amount))
  saldo.waiting_funds.amount = format(saldo.waiting_funds.amount)
  saldo.available.amount = format(saldo.available.amount)
  saldo.transferred.amount = format(saldo.transferred.amount)
  let translations = await Translation.find({}).or([{ tradutor: req.user._id }, { user: req.user._id }]).sort({_id: -1})
  if (req.user.is_tradutor) {
    _.each(translations, function (o) {
      o.deadline = moment(o.deadline).format('DD/MM/YYYY')
      o.price = _.replace(o.price, ',', '.')
      o.created_at = moment(o.created_at).fromNow()
      o.price = ((o.price / 100) * 50).toFixed(2)
    })
  } else {
    _.each(translations, function (o) {
      o.deadline = moment(o.deadline).format('DD/MM/YYYY')
      o.created_at = moment(o.created_at).fromNow()
    })
  }
  res.render('Payment/index.html', {current_user: req.user, saldo, translations})
})

router.get('/template', function (req, res, next) {
  res.render('Templates/template.html')
})

router.get('/faq', Auth, function (req, res, next) {
  res.render('Main/faq.html', {current_user: req.user})
})

router.get('/tradutor/registrar', function (req, res, next) {
  Language.find({deleted: false}).lean()
    .then(languages => {
      res.render('Auth/registerTradutor.html', {languages: languages})
    })
})

router.get('/nfeteste', function (req, res) {
  nfe('igor martins', 'igorpollo@gmail.com', '10,00')
  res.send('ok')
})

router.post('/register', async (req, res) => {
  let user = await User.find({email: req.body.email})
  let activateCode = makeid(20)
  req.body.created_at = moment().format('DD/MM/YYYY')
  req.body.activate_code = activateCode
  if (user.length > 0) {
    req.session.flash = 'Email já cadastrado'
    res.redirect('/registrar')
  } else {
    User.create(req.body)
      .then(user => {
        var session = req.session
        session.user = user._id
        sendMail(user.email, `Bem vindo Universo das Traduções`, `<h3>Olá, ${user.name}</h3><p>Para confirmar o seu cadastro, clique no link: <a href="${req.protocol + '://' + req.get('host')}/activate/${activateCode}">Ativar minha conta</a></p>`)
        res.redirect('/activate')
      })
      .catch(err => {
        console.log(err)
      })
  }
})

router.get('/activate/:key', async (req, res) => {
  let user = await User.findOne({activate_code: req.params.key})
  let session = req.session
  if (Object.keys(user).length !== 0) {
    await User.updateOne({_id: user._id}, {active: true})
    session.user = user._id
    if (req.session.page) {
      res.redirect(req.session.page)
    } else {
      res.redirect('/')
    }
  } else {
    res.redirect('/login')
  }
})

router.get('/activate', function (req, res) {
  res.render('Auth/activate.html')
})

router.get('/login', function (req, res, next) {
  let url = oauth2Client.generateAuthUrl({
    access_type: 'offline',
    scope: scopes
  })
  res.render('Auth/login.html', {attempt: req.query.attempt, url})
})

router.post('/login', function (req, res, next) {
  User.findOne()
    .where('email').equals(req.body.email)
    .where('password').equals(req.body.password)
    .then(user => {
      if (user) {
        console.log(user)
        var session = req.session
        session.user = user._id
        res.redirect('/translations')
      } else {
        res.redirect('/login?attempt=true')
      }
    })
})

router.post('/login2', function (req, res, next) {
  User.findOne()
    .where('email').equals(req.body.email)
    .where('password').equals(req.body.password)
    .then(user => {
      if (user) {
        console.log(user)
        var session = req.session
        session.user = user._id
        res.json({status: 'success', user: user})
      } else {
        res.redirect('/login?attempt=true')
      }
    })
})

router.get('/googlelogin', async (req, res) => {
  let code = req.query.code
  let session = req.session
  const {tokens} = await oauth2Client.getToken(code)
  oauth2Client.setCredentials(tokens)
  const plus = google.plus({
    version: 'v1',
    auth: oauth2Client
  })
  const user = await plus.people.get({userId: 'me'})
  let usuario = await User.findOne({email: user.data.emails[0].value})
  console.log(usuario)
  let image = user.data.image.url
  image = image.replace(/\?(.*)/, '')
  if (usuario) {
    await User.updateOne({email: usuario.email}, {
      picture: image,
      name: user.data.displayName,
      google_id: user.data.id
    })
    session.user = usuario._id
    res.redirect('/')
  } else {
    let newUser = await User.create({
      email: user.data.emails[0].value,
      picture: image,
      active: true,
      password: Math.random().toString(36).substring(7),
      name: user.data.displayName
    })
    sendMail(newUser.email, 'Bem vindo Universo Traduções', '<h3>Olá, Bem vindo Universo</h3><p>Etamos felizes por você ter se cadastrado.')
    session.user = newUser._id
    res.redirect('/')
  }
})

router.get('/forgotpassword', async (req, res) => {
  res.render('Auth/forgotPassword.html')
})

router.post('/forgotpassword', async (req, res) => {
  let password = makeid(20)
  let user
  try {
    let user = User.findOne({email: req.body.email})
    if(!user.email) throw new Error('error')
    user = await User.findOneAndUpdate({email: req.body.email}, {reset_pw_key: password})
  } catch(err) {
    req.session.flash = 'Não foi possível recuperar a senha'
    res.redirect('/login')
    return
  }
  sendMail(req.body.email, 'Recuperação de Senha', '<h3>Olá, ' + user.name + '</h3><p>Você solicitou uma redefinição de senha. Se você realmente fez essa solicitação, clique no link abaixo para definir uma nova senha</p><p><a href="https://plataforma.universotraducoes.com/resetpassword/' + password + '">Clique para redefinir a senha</a></p>')
  req.session.flash = 'Email enviado com sucesso!'
  res.redirect('/login')
})

router.get('/resetpassword/:resetid', async (req, res) => {
  if (req.query.error) {
    res.render('Auth/resetPassword.html', {error: req.query.error})
  } else {
    res.render('Auth/resetPassword.html', {resetid: req.params.resetid})
  }
})

router.post('/resetpassword/:resetid', async (req, res) => {
  let session = req.session
  let resetId = req.params.resetid
  if (req.body.password !== req.body.password_confirmation) {
    return res.redirect('/resetpassword/' + resetId)
  }
  let user = await User.findOneAndUpdate({reset_pw_key: resetId}, {password: req.body.password})
  sendMail(user.email, 'Recuperação de Senha', 'Olá ' + user.name + '. Sua senha foi redefinida.')
  session.user = user._id
  res.redirect('/')
})

router.post('/logout', function (req, res, next) {
  req.session.destroy(function (err) {
    if (err) { console.log(err) }
    res.redirect('/login')
  })
})

router.get('/logout', function (req, res, next) {
  req.session.destroy(function (err) {
    if (err) { console.log(err) }
    res.redirect('/login')
  })
})

// TEST ROUTES
router.get('/comentario', function (req, res) {
  res.send('test')
})

router.get('/secure', Auth, function (req, res) {
  res.send('Conseguiu acesso! Usuario: ' + req.user.name)
})

router.get('/session', function (req, res) {
  res.send(req.session.user)
})

router.get('/ola', function (req, res) {
  req.session.user = 'igorpollo'
  res.send('boa!')
})

router.post('/franc', async (req, res) => {
  let text = req.body.text
  let lang = franc.all(text)
  res.json({language: lang[0][0]})
})

router.post('/documents', upload.single('document'), function (req, res) {
  console.log(req.file)
  if (req.file.mimetype === 'application/pdf' || 
  req.file.mimetype === 'application/msword' || 
  req.file.mimetype === 'application/vnd.openxmlformats-officedocument.wordprocessingml.document' || 
  req.file.mimetype === 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' || 
  req.file.mimetype === 'application/vnd.openxmlformats-officedocument.presentationml.presentation' || 
  req.files.mimetype === 'application/vnd.ms-powerpoint' || 
  req.file.mimetype === 'application/vnd.ms-excel') {
    console.log('permitido')
    if (req.file.mimetype === 'application/pdf') {
      let pdfParser = new PDFParser(this, 1)

      pdfParser.on('pdfParser_dataError', errData => console.log(errData))
      pdfParser.on('pdfParser_dataReady', function (pdf) {
        var texto = pdfParser.getRawTextContent().replace(/-+Page \(\d+\) Break-+/gm, '')
        let lang = franc.all(texto)
        res.json({success: true, words: count(texto), file: req.file.filename, language: lang[0][0]})
      })
      pdfParser.loadPDF('uploads/' + req.file.filename)
    } 
    else if (req.file.mimetype == 'application/vnd.ms-excel' || req.file.mimetype == 'application/vnd.openxmlformats-officedocument.presentationml.presentation' || req.file.mimetype == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet') {
      textract.fromFileWithPath('uploads/' + req.file.filename, function( error, text ) {
        if(error) { console.log}
        let lang = franc.all(text)
        res.json({success: true, words: count(text), file: req.file.filename, language: lang[0][0]})
      })
    } else {
      if (req.file.mimetype === 'application/vnd.openxmlformats-officedocument.wordprocessingml.document') {
        mammoth.extractRawText({path: 'uploads/' + req.file.filename})
          .then(function (result) {
            let lang = franc.all(result.value.toString())
            res.json({success: true, file: req.file.filename, words: count(result.value.toString()), language: lang[0][0]})
          })
      } else {
        var extractor = new WordExtractor()

        var extracted = extractor.extract('uploads/' + req.file.filename)
        extracted.then(function (doc) {
          var texto = doc.getBody()
          let lang = franc.all(texto)
          console.log(lang[0][0])
          res.json({success: true, words: count(texto), file: req.file.filename, language: lang[0][0]})
        })
      }
    }
  } else {
    console.log('oi')
    res.json({success: false, message: 'arquivo não permitido'})
  }
})

router.post('/documents2', upload.array('documents', 12), async (req, res) => {
  var numWords = 0
  for (let i = 0; i < req.files.length; i++) {
    if (req.files[i].mimetype === 'application/pdf') {
      let pdfParser = new PDFParser(this, 1)

      pdfParser.on('pdfParser_dataError', errData => console.log(errData))
      pdfParser.on('pdfParser_dataReady', function (pdf) {
        console.log(pdf)
        var texto = pdfParser.getRawTextContent().replace(/-+Page \(\d+\) Break-+/gm, '')
        let lang = franc.all(texto)
        console.log(lang)
        numWords += count(texto)
      })
      pdfParser.loadPDF('uploads/' + req.files[i].filename)
    } else if (req.files[i].mimetype === 'application/vnd.openxmlformats-officedocument.wordprocessingml.document') {
      mammoth.extractRawText({path: 'uploads/' + req.files[i].filename})
        .then(function (result) {
          console.log(result.value.toString())
          numWords += count(result.value.toString())
        })
    } else if (req.files[i].mimetype === 'application/msword') {
      var extractor = new WordExtractor()

      var extracted = extractor.extract('uploads/' + req.files[i].filename)
      extracted.then(function (doc) {
        var texto = doc.getBody()
        let lang = franc.all(texto)
        console.log(lang)
        numWords += count(texto)
      })
    } else {
      return res.json({success: false, message: 'arquivo não permitido'})
    }
  }
  res.json({success: true, words: numWords})
})

router.get('/pdf', function (req, res) {
  let pdfParser = new PDFParser(this, 1)

  pdfParser.on('pdfParser_dataError', errData => console.log(errData))
  pdfParser.on('pdfParser_dataReady', function (pdf) {
    console.log(pdf)
    var texto = pdfParser.getRawTextContent().replace(/-+Page \(\d+\) Break-+/gm, '')
    res.json(count(texto))
  })

  pdfParser.loadPDF('./20131231103232738561744.pdf')
})

router.get('/word', function (req, res) {
  var extractor = new WordExtractor()

  var extracted = extractor.extract('./TestWordDoc.doc')
  extracted.then(function (doc) {
    var texto = doc.getBody()
    res.json(count(texto))
  })
})

router.get('/termo-privacidade', function (req, res) {
  res.render('Main/privacidade.html')
})

router.get('/termo-condicoes', function (req, res) {
  res.render('Main/termos.html')
})

router.get('/termos-tradutor', function (req, res) {
  res.render('Main/termos-tradutor.html')
})

router.get('/sendmail', function (req, res) {
  sendMail('igorpollo@gmail.com', 'Teste de email', '<h3>Olá, Igor Cesar</h3><p>Eu sou um email teste escrevendo algum texto para testar o email</p>')
})

module.exports = router
