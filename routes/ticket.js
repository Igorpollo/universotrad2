var express = require('express')
var router = express.Router()
var Auth = require('../middlewares/auth')
var User = require('../models/user')
var Ticket = require('../models/ticket')
var moment = require('moment')
var sendMail = require('../utils/sendMail')

router.get('/', Auth, function (req, res, next) {
  var message = false
  req.query.success ? message = true : message = false
  Ticket.find({user: req.user._id})
    .then(tickets => {
      res.render('Ticket/index.html', {current_user: req.user, message: message, tickets: tickets})
    })
})

router.post('/', Auth, function (req, res, next) {
  req.body.user = req.user._id
  req.body.created_at = moment().format('DD/MM/YYYY')
  Ticket.create(req.body)
    .then(ticket => {
      sendMail('atendimento@universotraducoes.com', 'Novo ticket aberto!', 'Um novo ticket foi aberto. Para visualiza-lo acesse <a href="https://plataforma.universotraducoes.com/admin/tickets">Admin Tickets</a>')
      res.redirect('/tickets')
    })
})

module.exports = router
