var express = require('express')
var router = express.Router()
var Auth = require('../middlewares/auth')
var User = require('../models/user')
var Translation = require('../models/translation')
var moment = require('moment')
var multer = require('multer')
var pagarme = require('pagarme')

var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, 'uploads/')
  },
  filename: function (req, file, cb) {
    cb(null, Date.now() + '-' + file.originalname)
  }
})

var upload = multer({ storage: storage })

/* GET users listing. */
router.get('/', function (req, res, next) {
  res.send('respond with a resource')
})

router.get('/profile', Auth, async (req, res, next) => {
  var message = false
  req.query.success ? message = true : message = false
  let translations
  let positiveAvaliations
  if (req.user.is_tradutor) {
    translations = await Translation.find({tradutor: req.user._id, avaliacao: true}).count()
    positiveAvaliations = await Translation.find({tradutor: req.user._id, avaliacao: true, avaliacao_qualidade: '1'}).count()
  }
  let avaliacao
  if (translations !== 0) {
    avaliacao = ((positiveAvaliations / translations) * 10).toFixed(1)
  } else {
    avaliacao = 10.0
  }
  res.render('User/profile.html', {current_user: req.user, message: message, languages: req.user.languages, avaliacao})
})

router.post('/profile', Auth, upload.fields([{ name: 'picture', maxCount: 1 }, { name: 'curriculum', maxCount: 8 }]), async (req, res, next) => {
  if (req.files.picture) {
    req.body.picture = 'https://plataforma.universotraducoes.com/' + req.files.picture[0].filename
  }
  if (req.files.curriculum) {
    console.log('tem curriculum')
    console.log(req.files.curriculum[0].filename)
    req.body.curriculum = req.files.curriculum[0].filename
    console.log(req.body.curriculum)
  }
  if (req.user.is_tradutor) {
    let client
    try {
      client = await pagarme.client.connect({
        api_key: process.env.PAGARME || 'ak_test_alEqoLOSSF7cTrl929fYbCzV0TbzH8'
      })
    } catch (err) {
      console.log(err)
    }

    let bank
    try {
      bank = await client.bankAccounts.create({
        bank_code: req.body.banco,
        agencia: req.body.agencia.trim(),
        conta: req.body.conta.trim(),
        conta_dv: req.body.conta_dg.trim(),
        legal_name: req.body.nome_completo,
        document_number: req.body.cpf_cnpj
      })
    } catch (err) {
      console.log(err.response.errors)
    }

    let recipient
    try {
      recipient = await client.recipients.create({
        bank_account_id: bank.id,
        transfer_interval: 'weekly',
        transfer_day: 5,
        transfer_enabled: true,
        postback_url: 'https://plataforma.universotraducoes.com/postback/recipient'
      })
    } catch (err) {
      console.log(err)
    }
    req.body.recebedor_id = recipient.id
  }
  await User.findByIdAndUpdate(req.user._id, req.body)
  res.redirect('/users/profile?success=true')
})

module.exports = router
