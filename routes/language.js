var express = require('express')
var router = express.Router()
var Language = require('../models/language')
var ServiceConfig = require('../models/serviceconfig')

/* GET users listing. */
router.get('/json', function (req, res, next) {
  Language.find().where('deleted').ne(true)
    .then(languages => {
      res.json(languages)
    })
})

router.get('/matchlanguages/:id', async (req, res) => {
  let services = await ServiceConfig.find({lang_origem: req.params.id}).populate('lang_origem').populate('lang_dest')
  let languages = services.map(item => item.lang_dest[0])
  res.json([...new Set(languages)])
})

router.get('/:id', function (req, res) {
  Language.findOne({_id: req.params.id})
    .then(language => {
      res.json(language)
    })
})

module.exports = router
