var mongoose = require('mongoose')
var Schema = mongoose.Schema

var languageSchema = new Schema({
  name: {type: String, required: true},
  abbreviation: {type: String},
  iso: String
})

const Language = mongoose.model('Language', languageSchema)
module.exports = Language
