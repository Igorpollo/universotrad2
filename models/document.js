var mongoose = require('mongoose')
var Schema = mongoose.Schema

var documentSchema = new Schema({
  name: {type: String, required: true}
})

const Document = mongoose.model('Document', documentSchema)
module.exports = Document
