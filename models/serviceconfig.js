var mongoose = require('mongoose')
var Schema = mongoose.Schema

var serviceconfigSchema = new Schema({
  name: {type: String, required: true},
  lang_origem: [{type: Schema.Types.ObjectId, ref: 'Language'}],
  lang_dest: [{type: Schema.Types.ObjectId, ref: 'Language'}],
  price: {type: String, required: true},
  qualidade: {type: String, required: true},
  tipo_de_servico: {type: String, required: true},
  limite_palavras1: {type: String, required: true},
  limite_palavras2: {type: String, required: true},
  limite_palavras3: {type: String, required: true},
  prazo1: {type: String, required: true},
  prazo2: {type: String, required: true},
  prazo3: {type: String, required: true},
  prazo: {type: String, required: true}
})

const Serviceconfig = mongoose.model('Serviceconfig', serviceconfigSchema)
module.exports = Serviceconfig
