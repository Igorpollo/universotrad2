var mongoose = require('mongoose')
var Schema = mongoose.Schema
var uniqid = require('uniqid')
var moment = require('moment')

var ticketSchema = new Schema({
  title: {type: String},
  description: {type: String},
  user: {type: Schema.Types.ObjectId, ref: 'User'},
  uid: {type: String, default: uniqid()},
  status: {type: String, default: 'Novo'},
  response: {type: String},
  created_at: {type: String}
})

const Ticket = mongoose.model('Ticket', ticketSchema)
module.exports = Ticket
