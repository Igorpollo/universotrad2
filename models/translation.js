var mongoose = require('mongoose')
var Schema = mongoose.Schema

var translationSchema = new Schema({
  title: {type: String},
  price: {type: String},
  deadline: {type: String},
  uid: {type: String},
  words: {type: String},
  tradutor: {type: Schema.Types.ObjectId, ref: 'User'},
  text: {type: String},
  translated_text: {type: String},
  translated_files: {type: Array},
  resumo: {type: String},
  avaliacao: {type: Boolean, default: false},
  completed_at: {type: String},
  no_prazo: {type: Boolean},
  glossario: String,
  revisao: {type: Boolean, default: false},
  transaction: {
    status: String,
    refuse_reason: String,
    date_created: String,
    id: String,
    card_brand: String,
    card_last_digits: String,
    payment_method: String
  },
  boleto_url: String,
  avaliacao_qualidade: String,
  avaliacao_preco: String,
  avaliacao_obs: String,
  avaliacao_prazo: String,
  avaliacao_flexibilidade: String,
  avaliacao_usabilidade: String,
  avaliacao_pagamento: String,
  tipo_de_documento: String,
  info_extra: String,
  outof_deadline: {type: Boolean},
  public: {type: Boolean, default: false},
  status: {type: String, required: true, default: 'Aguardando Pagamento'},
  user: {type: Schema.Types.ObjectId, ref: 'User'},
  document: {type: String},
  files: {type: Array},
  created_at: {type: String},
  serviceconfig: {type: Schema.Types.ObjectId, ref: 'Serviceconfig'}
})

const Translation = mongoose.model('Translation', translationSchema)
module.exports = Translation
