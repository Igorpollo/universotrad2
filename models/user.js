var mongoose = require('mongoose')
var Schema = mongoose.Schema
var moment = require('moment')

var userSchema = new Schema({
  email: {
    type: String,
    unique: true
  },
  picture: {type: String, default: 'http://www.europe-together.eu/wp-content/themes/sd/images/user-placeholder.svg'},
  password: {type: String, required: true},
  name: {type: String, required: true},
  is_tradutor: {type: Boolean, default: false, required: true},
  is_admin: {type: Boolean, default: false},
  is_premium: {type: Boolean, default: false},
  plain_pw: {type: String},
  nascimento: String,
  uid: String,
  banco: String,
  agencia: String,
  google_id: String,
  facebook_id: String,
  agencia_dg: String,
  conta: String,
  conta_dg: String,
  cpf_cnpj: String,
  nome_completo: String,
  recebedor_id: '',
  short_description: String,
  nome: String,
  cpf: String,
  deleted: Boolean,
  cnpj: String,
  inscricao_municipal: String,
  inscricao_estadual: String,
  cep: String,
  endereco: String,
  numero: String,
  complemento: String,
  bairro: String,
  cidade: String,
  telefone: String,
  celular: String,
  curriculum: String,
  phone: String,
  active: {type: Boolean, default: false},
  reset_pw_key: String,
  valor_palavra: String,
  activate_code: String,
  created_at: String,
  languages: [{type: Schema.Types.ObjectId, ref: 'Language'}],
  data_cadastro: String,
  area: String,
  nativo: String,
  pais: String,
  skype: String,
  paypal: String,
  area_teste: String,
  idioma_teste: String,
  nota_teste: String,
  revisor_teste: String,
  comentario_teste: String
})

userSchema.index({ email: 1, type: -1 })
const User = mongoose.model('User', userSchema)
module.exports = User
