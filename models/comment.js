var mongoose = require('mongoose')
var Schema = mongoose.Schema

var commentSchema = new Schema({
  text: {type: String, required: true},
  user: {type: Schema.Types.ObjectId, ref: 'User'},
  translation: {type: Schema.Types.ObjectId, ref: 'Translation'},
  is_review: {type: Boolean, default: false},
  time: {type: String}
})

const Comment = mongoose.model('Comment', commentSchema)
module.exports = Comment
